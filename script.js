let onSwipe = (target, direction) => {
    switch (direction) {
        case 'right':
            target.innerHTML = 'Swipe right';
            break;
        case 'left':
            target.innerHTML = 'Swipe left';
            break;
        case 'up':
            target.innerHTML = 'Swipe up';
            break;
        case 'down':
            target.innerHTML = 'Swipe down';
            break;
    }
};

let onAddSswipeTarget = function(e) {
    e.animationName === 's-swipe__target' && sSwipe(e.target, onSwipe);
};
document.addEventListener('animationstart', onAddSswipeTarget, false);
document.addEventListener('MSAnimationStart', onAddSswipeTarget, false);
document.addEventListener('webkitAnimationStart', onAddSswipeTarget, false);
