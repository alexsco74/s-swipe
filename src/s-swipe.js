let sSwipe = function (target, callback) {
    let start = {};
    let end = {};
    let tracking = false;
    let thresholdTime = 500;
    let thresholdDistance = 100;

    let gestureStart = function (e) {
        if (e.touches.length > 1) {
            tracking = false;
            return;
        } else {
            tracking = true;
            /* Hack - would normally use e.timeStamp but it's whack in Fx/Android */
            start.t = new Date().getTime();
            start.x = e.targetTouches[0].clientX;
            start.y = e.targetTouches[0].clientY;
        }
    };
    let gestureMove = function (e) {
        if (tracking) {
            e.preventDefault();
            end.x = e.targetTouches[0].clientX;
            end.y = e.targetTouches[0].clientY;
        }
    };
    let gestureEnd = function (e) {
        tracking = false;
        let now = new Date().getTime();
        let deltaTime = now - start.t;
        let deltaX = end.x - start.x;
        let deltaY = end.y - start.y;

        /* work out what the movement was */
        if (deltaTime > thresholdTime) {

            /* gesture too slow */
            return;
        } else {
            if ((deltaX > thresholdDistance) && (Math.abs(deltaY) < thresholdDistance)) {
                callback(e.currentTarget, 'right');
            } else if ((-deltaX > thresholdDistance) && (Math.abs(deltaY) < thresholdDistance)) {
                callback(e.currentTarget, 'left');
            } else if ((deltaY > thresholdDistance) && (Math.abs(deltaX) < thresholdDistance)) {
                callback(e.currentTarget, 'down');
            } else if ((-deltaY > thresholdDistance) && (Math.abs(deltaX) < thresholdDistance)) {
                callback(e.currentTarget, 'up');
            }
        }
    };

    target.addEventListener('touchstart', gestureStart, false);
    target.addEventListener('touchmove', gestureMove, false);
    target.addEventListener('touchend', gestureEnd, false);
};